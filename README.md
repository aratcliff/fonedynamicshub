Fone Dynamics Hub
=================

A collection of screenshots of the developed documentation site Fone Dynamics Hub which is yet to be published. All work was developed in Vue.js

# Fone Dynamics Hub pages

### Example of an article page

![Article page](https://bitbucket.org/aratcliff/fonedynamicshub/raw/258ae5bc24f3c5e92ee7f176613e622b0245018a/Screenshots/Article_4.PNG)

### Example of a scrolled down article page, containing code blocks and page footers

![Article page](https://bitbucket.org/aratcliff/fonedynamicshub/raw/258ae5bc24f3c5e92ee7f176613e622b0245018a/Screenshots/Article_3.PNG)

### Example of changelog filled in with dummy points

![Changelog](https://bitbucket.org/aratcliff/fonedynamicshub/raw/258ae5bc24f3c5e92ee7f176613e622b0245018a/Screenshots/Changelog.PNG)

# Fone Dynamics Hub CMS pages

### Article editor examples

![Article editor](https://bitbucket.org/aratcliff/fonedynamicshub/raw/258ae5bc24f3c5e92ee7f176613e622b0245018a/Screenshots/Editor_1.PNG)

![Article editor](https://bitbucket.org/aratcliff/fonedynamicshub/raw/258ae5bc24f3c5e92ee7f176613e622b0245018a/Screenshots/Editor_3.PNG)

### Article browser expanded

![Article browser](https://bitbucket.org/aratcliff/fonedynamicshub/raw/258ae5bc24f3c5e92ee7f176613e622b0245018a/Screenshots/List_1.PNG)

### Article browser collapsed

![Article browser](https://bitbucket.org/aratcliff/fonedynamicshub/raw/258ae5bc24f3c5e92ee7f176613e622b0245018a/Screenshots/List_2.PNG)